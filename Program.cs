using System;

namespace tarea6_master
{
 /*
 Crear una clase Persona que tenga como atributos el "cedula, nombre, apellido y la edad (definir las propiedades para poder acceder a dichos atributos)". Definir como responsabilidad un método para mostrar ó imprimir. Crear una segunda clase Profesor que herede de la clase Persona. Añadir un atributo sueldo ( y su propiedad) y el método para imprimir su sueldo. Definir un objeto de la clase Persona y llamar a sus métodos y propiedades. También crear un objeto de la clase Profesor y llamar a sus métodos y propiedades.

     class Program
    {
        static void Main(string[] args)
        {
           class Persona
    {
        private string cedula;

        private string nombre;

        private string apellido;

        private int edad;

        public Persona(){
            Console.Write("Ingrese su cedula: ");
            cedula = Console.ReadLine();
            Console.Write("Ingrese su nombre: ");
            nombre = Console.ReadLine();
            Console.Write("Ingrese su apellido: ");
            apellido = Console.ReadLine();
            Console.Write("Ingrese su edad: ");
            string Linea = Console.ReadLine();
            edad = int.Parse(Linea);
        }

        public void Imprimir()
        {
            Console.WriteLine("Bienvenido " + nombre);
            Console.WriteLine("Cedula: " + cedula);
            Console.WriteLine("Nombre: " + nombre);
            Console.WriteLine("Apellido: " + apellido);
            Console.WriteLine("Edad: " + edad);
        }

     class Profesor  
        {
           private double Sueldo;

           public Profesor(){
               Sueldo = 98700;
           }

           public void Imprime()
           {
             Console.WriteLine("Su sueldo es: {0} ", Sueldo);  
           }

        }

        static void Main(string[] args)
        {
            Persona Persona01 = new Persona();
            Profesor Profesor01 = new Profesor();

            Persona01.Imprimir();
            Profesor01.Imprime();
        }
    }

Crear una clase Contacto. Esta clase deberá tener los atributos "nombre, apellidos, telefono y direccion". También deberá tener un método "SetContacto", de tipo void y con los parámetros string, que permita cambiar el valor de los atributos. También tendrá un método "Saludar", que escribirá en pantalla "Hola, soy " seguido de sus datos. Crear también una clase llamada ProbarContacto. Esta clase deberá contener sólo la función Main, que creará dos objetos de tipo Contacto, les asignará los datos del contacto y les pedirá que saluden.


public class contacto
    {
        private string nombre;
        private string apellidos;
        private string direccion;
        private int telefono;

        public void SetContacto(string nombre)
        {
            this.nombre = nombre;
            
        }

        public void saludar()
        {
            Console.WriteLine("Hola soy {0} ", nombre);
        }
    }

    public class ProbarContacto
    {
        public static void Main()
        {
            contacto cont = new contacto();
            cont.SetContacto("Elian Paulino");
            cont.saludar();
        }
    }




Crear tres clases ClaseA, ClaseB, ClaseC que ClaseB herede de ClaseA y ClaseC herede de ClaseB. Definir un constructor a cada clase que muestre un mensaje. Luego definir un objeto de la clase ClaseC.

  public class A
    {
        public A()
        {
            Console.WriteLine("Constructor de la clase A");
        }
    }

   public class B: A
    {
        public B()
        {
            Console.WriteLine("Constructor de la clase B");
        }
    }

    public class C: B
    {
        public C()
        {
            Console.WriteLine("Constructor de la clase C");
        }
    }

    class Objeto
    {
        static void Main (string[] args)
        {
            C obj = new C();
            Console.ReadKey();
        }
    }
}


*/




        }
    }
}
